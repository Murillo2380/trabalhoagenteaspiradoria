package com.ia.project.agente;

import com.ia.project.agente.estrategia.EstrategiaBusca;
import com.sun.istack.internal.NotNull;

/**
 * Classe que define comportamentos básicos de um agente.
 */
public abstract class Agente {

    /**
     * Estrategia usada por este agente para se mover.
     */
    private EstrategiaBusca estrategia;

    /**
     *
     * @param estrategia Estrategia a ser usada pelo agente para se mover no mapa.
     */
    public Agente(@NotNull EstrategiaBusca estrategia){
        this.estrategia = estrategia;
    }

    /**
     *
     * @param estrategia Nova estratégia a ser usada pelo agente para se mover.
     */
    public void setEstrategia(@NotNull EstrategiaBusca estrategia){
        this.estrategia = estrategia;
    }

    /**
     *
     * @return Estrategia usada por este agente para se mover.
     */
    public @NotNull EstrategiaBusca getEstrategia(){
        return estrategia;
    }

    /**
     *
     * @return Relatorio referente as atividades desenvolvidas pelo agente.
     */
    public abstract String relatorio();


}
