package com.ia.project.mapa;

import com.ia.project.agente.AgenteAspirador;
import com.ia.project.agente.AgenteMatriz;
import com.ia.project.agente.estrategia.EstrategiaBuscaMatriz;
import com.sun.glass.ui.SystemClipboard;

import java.awt.*;
import java.util.HashSet;

/**
 * Mapa que contém um agente e permite imprimir o mapa baseado nas informações de onde o agente já passou.
 */
public class MapaComAgente extends Mapa implements AgenteAspirador.SensorLimpeza, AgenteAspirador.MotorLimpeza {

    private AgenteMatriz agente;

    /**
     * {@link HashSet} permite adicionar vértices em um conjunto, indicando que o agente já passou por ali.
     */
    private HashSet<Point> posicoesVisitadas = new HashSet<>();

    /**
     * Cria um mapa {@code n} x {@code n}
     *
     * @param n Tamanho do mapa.
     */
    public MapaComAgente(int n) {
        super(n);
    }

    /**
     * Cria um mapa com as mesmas configurações do {@code mapa}.
     * @param mapa Mapa a ser clonado.
     */
    public MapaComAgente(MapaComAgente mapa){
        super(mapa);
    }

    public void setAgente(AgenteMatriz agente){
        posicoesVisitadas.clear(); // limpa histórico anterior
        this.agente = agente;
    }

    public AgenteMatriz getAgente(){
        return agente;
    }



    @Override
    public String toString(){

        int tamanho = getMapa().length;
        StringBuilder sb = new StringBuilder();

        EstrategiaBuscaMatriz estrategia = (EstrategiaBuscaMatriz) agente.getEstrategia();
        EstrategiaBuscaMatriz.Grafo g = estrategia.resultado();

        EstrategiaBuscaMatriz.Vertice verticeInicial = g.get(
                new EstrategiaBuscaMatriz.Vertice(estrategia.getLinhaInicial(), estrategia.getColunaInicial()));

        verificarPosicoes(verticeInicial);

        Point pontoAuxiliar = new Point();
        Point posicaoAgente = new Point(agente.getLinhaAtual(), agente.getColunaAtual());

        for(int i = 0; i < tamanho + 2; i++)
            sb.append("*");

        sb.append(System.lineSeparator());

        for(int i = 0; i < tamanho; i++){ // passa por todas as linhas

            sb.append("*");

            for(int j = 0; j < tamanho; j++){ // passa por todas as colunas

                pontoAuxiliar.x = i;
                pontoAuxiliar.y = j;

                if(posicaoAgente.x == i && posicaoAgente.y == j){
                    sb.append("X");
                }

                else if(posicoesVisitadas.contains(pontoAuxiliar) == true){
                    sb.append("-");
                }

                else if(getMapa()[i][j] == ESPACO_OBSTACULO){
                    sb.append("O");
                }

                else if(getMapa()[i][j] == ESPACO_SUJEIRA){
                    sb.append("S");
                }

                else
                    sb.append(" ");

            }

            sb.append("*").append(System.lineSeparator()); // pula linha

        }

        for(int i = 0; i < tamanho + 2; i++)
            sb.append("*");

        for(int i = 0; i < 5; i++) // adiciona 5 espaços no final da impressão
            sb.append(System.lineSeparator());

        return sb.toString();
    }

    /**
     * Adiciona o vértice {@code vertice} no conjunto de {@link #posicoesVisitadas} indicando que aquela posição da matriz
     * já foi visitada pelo agente, chamando recursivamente esta função para todos os vértices em
     * {@link EstrategiaBuscaMatriz.Vertice#getVizinhos()}
     * @param vertice Vértice a ser verificado
     */
    private void verificarPosicoes(EstrategiaBuscaMatriz.Vertice vertice){

        posicoesVisitadas.add(new Point(vertice.getLinha(), vertice.getColuna()));

        for(EstrategiaBuscaMatriz.Vertice v : vertice.getVizinhos()){
            verificarPosicoes(v);
        }

    }

    @Override
    public boolean estaLimpo(AgenteAspirador agenteAspirador) {

        if(getMapa()[agenteAspirador.getLinhaAtual()][agenteAspirador.getColunaAtual()] == ESPACO_SUJEIRA)
            return false;

        else
            return true;

    }

    @Override
    public boolean limpar(AgenteAspirador agente) {
        if(getMapa()[agente.getLinhaAtual()][agente.getColunaAtual()] == ESPACO_SUJEIRA){
            limparMapa(getAgente().getLinhaAtual(),agente.getColunaAtual());
            return true;
        }

        return false;
    }


}
