package com.ia.project.agente;

/**
 * Interface que define uma movimentação horizontal e vertical
 */
public interface Movimentacao {

    /**
     * Move algo para cima.
     */
    void moverCima();

    /**
     * Move algo para baixo.
     */
    void moverBaixo();

    /**
     * Move algo para a esquerda.
     */
    void moverEsquerda();

    /**
     * Move algo para a direita.
     */
    void moverDireita();
}
