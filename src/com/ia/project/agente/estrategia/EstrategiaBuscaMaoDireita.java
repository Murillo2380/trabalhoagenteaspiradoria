package com.ia.project.agente.estrategia;

/**
 * Estrategia de busca seguindo a regra da mão direita (um dos algoritmos para andar em um labirinto)
 */
public class EstrategiaBuscaMaoDireita extends EstrategiaBuscaMatriz {

    /**
     * @see #calcularOlhar(int, int, int, int)
     */
    private static final int OLHANDO_DIREITA = 0;

    /**
     * @see #calcularOlhar(int, int, int, int)
     */
    private static final int OLHANDO_ESQUERDA = 1;

    /**
     * @see #calcularOlhar(int, int, int, int)
     */
    private static final int OLHANDO_CIMA = 2;

    /**
     * @see #calcularOlhar(int, int, int, int)
     */
    private static final int OLHANDO_BAIXO = 3;

    public EstrategiaBuscaMaoDireita(Mapeamento mapeamento) {
        super(mapeamento);
    }

    /**
     *
     * @param i Linha da recursão.
     * @param j Coluna da recursão.
     * @param pai Vértice que contém o {@code i} anterior e {@code j}.
     */
    private void buscaRecursiva(int i, int j, Vertice pai){

        Vertice novoVertice = new Vertice(i, j);

        if(getGrafo().containsValue(novoVertice)) // verifica se existe no grafo.
            return;

        if(getMapeamento().validoParaMovimentacao(i, j)) {
            getPassoBusca().novoPassoBusca(i, j);

            pai.addVizinho(novoVertice);
            getGrafo().put(novoVertice, novoVertice);

            switch(calcularOlhar(i, j, pai.getLinha(), pai.getColuna())){

                case OLHANDO_CIMA:
                    //System.out.println("OLHANDO CIMA");
                    buscaRecursiva(i, j + 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i - 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i, j - 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    break;

                case OLHANDO_BAIXO:
                    //System.out.println("OLHANDO BAIXO");
                    buscaRecursiva(i, j - 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i + 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i, j + 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    break;

                case OLHANDO_ESQUERDA:
                    //System.out.println("OLHANDO ESQUERDA");
                    buscaRecursiva(i - 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i, j - 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i + 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    break;

                case OLHANDO_DIREITA:
                    //System.out.println("OLHANDO DIREITA");
                    buscaRecursiva(i + 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i, j + 1, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    buscaRecursiva(i - 1, j, novoVertice);
                    getPassoBusca().novoPassoBusca(i, j); // avisa que voltou

                    break;

            }


        }

    }

    @Override
    public void realizarBusca() {

        // considera inicialmente que está olhando para baixo.

        Vertice novoVertice = new Vertice(getLinhaInicial(), getColunaInicial());
        getGrafo().put(novoVertice, novoVertice);

        buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() - 1, novoVertice);
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou

        buscaRecursiva(novoVertice.getLinha() + 1, novoVertice.getColuna(), novoVertice);
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou

        buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() + 1, novoVertice);
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou

    }

    /**
     * Calcula onde um objeto estaria olhando baseado na coordenada matricial anterior.
     * @param i Posição da linha atual
     * @param j Posição da coluna atual
     * @param iAnterior Posição da linha anterior.
     * @param jAnterior Posição da coluna anterior.
     * @return
     *  <p>{@link #OLHANDO_BAIXO} caso {@code i > iAnterior}   e {@code j == jAnterior}</p>
     *  <p>{@link #OLHANDO_ESQUERDA} caso {@code i == iAnterior}   e {@code j < jAnterior}</p>
     *  <p>{@link #OLHANDO_DIREITA} caso {@code i == iAnterior}   e {@code j > jAnterior}</p>
     *  <p>{@link #OLHANDO_CIMA} caso contrário</p>
     */
    private int calcularOlhar(int i, int j, int iAnterior, int jAnterior){

        if(i == iAnterior && j < jAnterior){
            return OLHANDO_ESQUERDA;
        }

        else if(i == iAnterior && j > jAnterior){
            return OLHANDO_DIREITA;
        }

        else if(j == jAnterior && i > iAnterior){
            return OLHANDO_BAIXO;
        }

        else return OLHANDO_CIMA;
    }
}

