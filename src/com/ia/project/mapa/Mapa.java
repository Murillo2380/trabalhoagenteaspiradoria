package com.ia.project.mapa;

import com.ia.project.agente.Agente;
import com.ia.project.agente.AgenteMatriz;
import com.ia.project.agente.estrategia.EstrategiaBuscaMatriz;

import java.util.Arrays;
import java.util.Random;

/**
 * Classe que define atributos básicos de um mapa quadricular.
 */
public class Mapa implements EstrategiaBuscaMatriz.Mapeamento{

    /**
     * Caso este seja o valor em uma posição do mapa, aquele espaço estará vazio.
     */
    public static final byte ESPACO_LIVRE = 0;

    /**
     * Caso este seja o valor em uma posição do mapa, aquele espaço conterá uma sujeira.
     */
    public static final byte ESPACO_SUJEIRA = 1;

    /**
     * Caso este seja o valor em uma posição do mapa, aquele espaço conterá um obstáculo.
     */
    public static final byte ESPACO_OBSTACULO = 2;

    private final byte mapa[][];

    private int quantidadeLugaresSujos = 0;

    /**
     * Cria um mapa {@code n} x {@code n}
     * @param n Tamanho do mapa.
     */
    public Mapa(int n){
        this.mapa = new byte[n][n];
    }

    /**
     * Cria um mapa clonando o mapa passado para a função.
     * @param mapa Mapa a ser clonado.
     */
    public Mapa(Mapa mapa){
        byte[][] mapaParaClonar = mapa.getMapa();

        this.mapa = new byte[mapaParaClonar.length][];

        for(int i = 0; i < mapaParaClonar .length; i++) {
            this.mapa[i] = Arrays.copyOf(mapaParaClonar [i], mapaParaClonar [i].length);
        }

    }



    /**
     * Passa por cada posição livre do mapa e sorteia entre 0 a 100 um valor que representará
     * a chance em porcentagem de aquele local receber um obstáculo.
     * @param chance Chance de aparecer um obstáculo em algum lugar do mapa. Quanto menor o valor, menor a chance
     *               logo menos obstáculos tenderão a aparecer. Se for maior que {@code 100}, esta função retornará
     *               imediatamente, o mesmo se for menor ou igual a {@code 0}.
     */
    public void gerarObstaculos(int chance){
        if(chance > 100 || chance <= 0) // cancela operação
            return;

        Random random = new Random();

        for(int i = 0; i < mapa.length; i++){
            for(int j = 0; j < mapa.length; j++){
                if(Math.abs(random.nextInt() % 100) <= chance){
                    mapa[i][j] = ESPACO_OBSTACULO;
                }
            }
        }
    }

    /**
     * Passa por cada posição livre do mapa e sorteia entre 0 a 100 um valor que representará
     * a chance em porcentagem de aquele local receber uma sujeira.
     * @param chance Chance de aparecer uma sujeira em algum lugar do mapa. Quanto menor o valor, menor a chance
     *               logo menos sujeiras tenderão a aparecer. Se for maior que {@code 100}, esta função retornará
     *               imediatamente, o mesmo se for menor ou igual a {@code 0}.
     */
    public void gerarSujeira(int chance) {

        if(chance > 100 || chance <= 0) // cancela operação
            return;

        Random random = new Random();

        for(int i = 0; i < mapa.length; i++){
            for(int j = 0; j < mapa.length; j++){
                if(Math.abs(random.nextInt() % 100) <= chance){
                    mapa[i][j] = ESPACO_SUJEIRA;
                    quantidadeLugaresSujos++;
                }
            }
        }

    }

    /**
     * Limpa uma certa posição do mapa.
     * @param i Linha para ser limpa.
     * @param j Coluna a ser limpa.
     */
    public void limparMapa(int i, int j){

        if(mapa[i][j] == ESPACO_SUJEIRA) {
            quantidadeLugaresSujos--;
            mapa[i][j] = ESPACO_LIVRE;
        }

    }

    /**
     *
     * @return Quantidade de sujeiras que ainda existe no mapa.
     */
    public int getSujeiraRestante(){
        return quantidadeLugaresSujos;
    }

    /**
     * Elimina todos os obstáculos e sujeiras do mapa;
     */
    public void limparMapa(){

        for(int i = 0; i < mapa.length; i++)
            for(int j = 0; j < mapa[i].length; j++)
                limparMapa(i, j);

    }

    /**
     *
     * @return Mapa gerado por esta classe.
     */
    public byte[][] getMapa(){
        return mapa;
    }

    @Override
    public boolean validoParaMovimentacao(int i, int j) {

        if(i < 0 || i >= numLinhas())
            return false;

        if(j < 0 || j >= numColunas())
            return false;

        if(mapa[i][j] == ESPACO_OBSTACULO)
            return false;

        return true;

    }

    @Override
    public int numLinhas() {
        return mapa.length;
    }

    @Override
    public int numColunas() {
        return mapa.length;
    }



}
