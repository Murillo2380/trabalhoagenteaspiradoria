package com.ia.project;

import com.ia.project.agente.*;
import com.ia.project.agente.estrategia.*;
import com.ia.project.mapa.Mapa;
import com.ia.project.mapa.MapaComAgente;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        MapaComAgente mapa = new MapaComAgente(scanner.nextInt());
        //scanner.close();

        mapa.gerarObstaculos(90);
        mapa.gerarSujeira(70);

        MapaComAgente mapa2 = new MapaComAgente(mapa); // clona o mapa.

        System.out.println("Mão direita");
        EstrategiaBuscaMatriz estrategia = new EstrategiaBuscaMaoDireita(mapa);

        AgenteMatriz agente = new AgenteAspirador(estrategia, mapa, mapa);
        mapa.setAgente(agente);

        estrategia.setPassoBusca(agente);
        estrategia.realizarBusca();

        System.out.println("Busca profundidade");

        estrategia = new EstrategiaBuscaProfundidade(mapa2);
        AgenteMatriz agente2 = new AgenteAspirador(estrategia, mapa2, mapa2);
        mapa2.setAgente(agente2);

        estrategia.setPassoBusca(agente2);
        estrategia.realizarBusca();

        System.out.println("Agente1" + System.lineSeparator() + agente.relatorio() + System.lineSeparator()
            + "Sujeira restante no mapa = " + mapa.getSujeiraRestante());
        System.out.println("Agente2" + System.lineSeparator() + agente2.relatorio() + System.lineSeparator()
            + "Sujeira restante no mapa = " + mapa.getSujeiraRestante());


    }


}
