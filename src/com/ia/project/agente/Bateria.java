package com.ia.project.agente;

/**
 * Interface que define métodos para algo que use energia
 */
public interface Bateria {

    /**
     *
     * @return Valor numérico representando o atual nível de energia.
     */
    int getEnergiaAtual();

    /**
     *
     * @param carga Valor alvo para carregar.
     */
    void carregar(int carga);



}
