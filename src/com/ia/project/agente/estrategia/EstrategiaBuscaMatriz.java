package com.ia.project.agente.estrategia;

import javafx.print.PaperSource;

import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * Classe que define atributos para uma busca que será realizada em uma matriz.
 */
public abstract class EstrategiaBuscaMatriz implements EstrategiaBusca<EstrategiaBuscaMatriz.Grafo> {

    private Mapeamento mapeamento;
    private PassoBusca passoBusca;
    private int linhaInicial;
    private int colunaInicial;
    private Grafo grafo = new Grafo();

    /**
     *
     * @return Grafo resultante da busca realizada por esta classe.
     */
    @Override
    public Grafo resultado(){
        return grafo;
    }

    /**
     *
     * @param mapeamento Matriz que representa um mapa.
     */
    public EstrategiaBuscaMatriz(Mapeamento mapeamento){
        this.mapeamento = mapeamento;
        this.linhaInicial = 0;
        this.colunaInicial = 0;

    }

    EstrategiaBuscaMatriz(Mapeamento mapeamento, int linhaInicial, int colunaInicial){
        this.mapeamento = mapeamento;
        this.linhaInicial = linhaInicial;
        this.colunaInicial = colunaInicial;
    }

    public void setPassoBusca(PassoBusca passoBusca){
        this.passoBusca = passoBusca;
    }

    public PassoBusca getPassoBusca(){
        return passoBusca;
    }

    /**
     *
     * @return Classe que contém as funções de {@link Mapeamento}.
     */
    public Mapeamento getMapeamento(){
        return mapeamento;
    }

    /**
     *
     * @return Linha em que a busca irá começar.
     * @see #getColunaInicial()
     */
    public int getLinhaInicial(){
        return linhaInicial;
    }

    /**
     *
     * @return Coluna em que a busca irá começar.
     * @see #getLinhaInicial()
     */
    public int getColunaInicial(){
        return colunaInicial;
    }

    protected Grafo getGrafo(){
        return grafo;
    }

    /**
     * Classe que define as propriedades de vértices em matrizes.
     */
    public static class Vertice{

        private int i;
        private int j;

        private LinkedHashSet<Vertice> vizinhos; // hashset não permite vizinhos repetidos

        /**
         *
         * @param i Linha deste vértice.
         * @param j Coluna deste vértice.
         */
        public Vertice(int i, int j){
            this.i = i;
            this.j = j;
            vizinhos = new LinkedHashSet<>();
        }

        /**
         *
         * @return Linha em que o vizinho está posicionado.
         */
        public int getLinha(){
            return i;
        }

        /**
         *
         * @return Coluna em que o vizinho está posicionado.
         */
        public int getColuna(){
            return j;
        }

        /**
         *
         * @return Conjunto de vizinhos.
         */
        public LinkedHashSet<Vertice> getVizinhos(){
            return vizinhos;
        }

        /**
         * Adiciona um vértice como vizinho se não existir.
         * @param v Vizinho a ser adicionado.
         */
        public void addVizinho(Vertice v){
            vizinhos.add(v);
        }

        @Override
        public boolean equals(Object o){
            if(o == null)
                return false;

            if(this.getClass().isAssignableFrom(o.getClass())) { // verifica se o parametro é do tipo Vertice
                Vertice v = (Vertice) o;
                return v.getColuna() == this.getColuna() && v.getLinha() == this.getLinha();
            }

            else return false;
        }

        @Override
        public int hashCode(){
            return 100*i + j;
        }


    }

    /**
     * Define que um grafo é um HashMap que mapeia um vértice para um vértice.
     */
    public class Grafo extends HashMap<Vertice, Vertice>{

    }


    /**
     * Contrato que forneça informações sobre uma certa posição de uma matriz.
     */
    public interface Mapeamento{

        /**
         *
         * @param i Linha da matriz a ser testada.
         * @param j Coluna da matriz a ser testada.
         * @return {@code true} se a posição {@code i} e {@code j} for uma posição válida para se mover, {@code @false}
         * caso aquela posição deva ser ignorada.
         */
        boolean validoParaMovimentacao(int i, int j);

        /**
         *
         * @return Inteiro positivo representando o número de linhas.
         */
        int numLinhas();

        /**
         *
         * @return Inteiro positivo representando o número de colunas.
         */
        int numColunas();

    }

    /**
     * Interface com funções que são chamadas ao decorrer da busca.
     */
    public interface PassoBusca{

        /**
         *
         * @param i Linha da matriz onde a busca está ocorrendo.
         * @param j Coluna da matriz onde a busca está ocorrendo.
         */
        void novoPassoBusca(int i, int j);

    }

}
