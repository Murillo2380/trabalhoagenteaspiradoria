package com.ia.project.agente;

import com.ia.project.agente.AgenteMatriz;
import com.ia.project.agente.Bateria;
import com.ia.project.agente.estrategia.EstrategiaBusca;
import com.ia.project.agente.estrategia.EstrategiaBuscaMatriz;
import com.ia.project.agente.estrategia.EstrategiaBuscaProfundidade;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;

/**
 * Agente capaz de limpar um ambiente.
 */
public class AgenteAspirador extends AgenteMatriz implements Bateria {

    /**
     * Carga mínima, ao atingir esse nível ele deve voltar para a base realizar o carregamento da bateria.
     */
    private static final int CARGA_MINIMA = 2;

    /**
     * Carga que deve ser atingida ao carregar.
     */
    private static final int CARGA_MAXIMA = 10;

    /**
     * Quantidade de energia atualmente no agente.
     */
    private int cargaAtual;

    private SensorLimpeza sensorLimpeza;

    private MotorLimpeza motorLimpeza;

    private ArrayList<EstrategiaBuscaMatriz.Vertice> filaPassos;

    /**
     * Contador de quantas limpezas foram realizadas.
     */
    private int numLimpezas;

    /**
     * Se for {@code true} então o robo está retornando para a base.
     */
    private boolean retornandoBase = false;

    /**
     * @param estrategia Estrategia a ser usada pelo agente para se mover no mapa.
     * @param sensorLimpeza Sensor cuja implementação permita dizer se onde o agente se encontra está limpo ou não.
     * @param motorLimpeza Motor cuja implementação deve permitir o agente limpar a região onde ele se encontra.
     */
    public AgenteAspirador(@NotNull EstrategiaBuscaMatriz estrategia, @NotNull SensorLimpeza sensorLimpeza,
                           MotorLimpeza motorLimpeza) {
        super(estrategia);
        this.sensorLimpeza = sensorLimpeza;
        this.motorLimpeza = motorLimpeza;
        numLimpezas = 0;
        cargaAtual = CARGA_MAXIMA;
        filaPassos = new ArrayList<>();
    }

    /**
     * @return Número de limpezas realizadas pelo robo.
     */
    public int getNumLimpezas(){
        return numLimpezas;
    }

    @Override
    public int getEnergiaAtual() {
        return cargaAtual;
    }

    @Override
    public void carregar(int carga) {
        System.out.println("Retornando para a base");
        retornandoBase = true;

        for(int i = filaPassos.size() - 1; i >= 0; i--){ // percorre a fila do fim para o início.
            super.novoPassoBusca(filaPassos.get(i).getLinha(), filaPassos.get(i).getColuna()); // diz que esse retorno faz parte da busca
        }

        EstrategiaBuscaMatriz estrategiaBuscaMatriz = (EstrategiaBuscaMatriz) getEstrategia();
        novoPassoBusca(estrategiaBuscaMatriz.getLinhaInicial(), estrategiaBuscaMatriz.getColunaInicial()); // coordenadas da base
        cargaAtual = carga; // carrega.

        for(EstrategiaBuscaMatriz.Vertice v : filaPassos) // percorre a fila do inicio ao fim.
            super.novoPassoBusca(v.getLinha(), v.getColuna());

        retornandoBase = false;

    }

    @Override
    public void novoPassoBusca(int i, int j){
        if(sensorLimpeza.estaLimpo(this) == false){ // se estiver limpo, limpa a região.
            if(motorLimpeza.limpar(this) == true){
                numLimpezas++; // incrementa se conseguiu limpar.
                cargaAtual--;

                if(getEnergiaAtual() <= CARGA_MINIMA){ // se estiver com pouca energia, é necessário voltar para a base.
                    carregar(CARGA_MAXIMA);
                }

            }
        }
        super.novoPassoBusca(i, j); // faz o que estava definido em AgenteMatriz
    }


    @Override
    public String relatorio() {
        return "Passos realizados: " + getNumPassos() + System.lineSeparator() +
                "Limpezas realizadas: " + getNumLimpezas();
    }

    @Override
    public void moverCima(){
        super.moverCima(); // faz o que fazia antes
        if(retornandoBase == false)
            filaPassos.add(new EstrategiaBuscaMatriz.Vertice(getLinhaAtual(), getColunaAtual()));
    }

    @Override
    public void moverBaixo(){
        super.moverBaixo(); // faz o que fazia antes
        if(retornandoBase == false)
            filaPassos.add(new EstrategiaBuscaMatriz.Vertice(getLinhaAtual(), getColunaAtual()));
    }

    @Override
    public void moverEsquerda(){
        super.moverEsquerda(); // faz o que fazia antes
        if(retornandoBase == false)
            filaPassos.add(new EstrategiaBuscaMatriz.Vertice(getLinhaAtual(), getColunaAtual()));
    }

    @Override
    public void moverDireita(){
        super.moverDireita(); // faz o que fazia antes
        if(retornandoBase == false)
            filaPassos.add(new EstrategiaBuscaMatriz.Vertice(getLinhaAtual(), getColunaAtual()));
    }

    /**
     * Interface que define métodos necessários para que o agente saiba que um lugar está limpo
     */
    public interface SensorLimpeza{

        /**
         *
         * @param agenteAspirador Agente que deseja saber se a posição que ele se encontra está limpo.
         * @return {@code true} caso esteja limpo, {@code false} caso contrário.
         */
        boolean estaLimpo(AgenteAspirador agenteAspirador);

    }

    /**
     * Interface que define métodos para que o agente possa limpar onde o agente se encontra.
     */
    public interface MotorLimpeza{

        /**
         *
         * @param agente Agente que vai limpar.
         * @return {@code true} se conseguiu limpar.
         */
        boolean limpar(AgenteAspirador agente);

    }

}
