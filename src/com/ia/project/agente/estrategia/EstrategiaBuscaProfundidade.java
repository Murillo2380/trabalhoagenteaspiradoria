package com.ia.project.agente.estrategia;


/**
 * Classe que contém o algoritmo busca em profundidade em uma matriz.
 */
public class EstrategiaBuscaProfundidade extends EstrategiaBuscaMatriz {

    public EstrategiaBuscaProfundidade(Mapeamento mapeamento){
        super(mapeamento);
    }

    /**
     *
     * @param i Linha da recursão.
     * @param j Coluna da recursão.
     * @param pai Vértice que contém o {@code i} anterior e {@code j}.
     */
    private void buscaRecursiva(int i, int j, Vertice pai){

        Vertice novoVertice = new Vertice(i, j);

        if(getGrafo().containsValue(novoVertice)) // verifica se existe no grafo.
            return;

        if(getMapeamento().validoParaMovimentacao(i, j)) {
            getPassoBusca().novoPassoBusca(i, j); // avisa que uma nova posição do mapa foi visitada.

            pai.addVizinho(novoVertice);
            getGrafo().put(novoVertice, novoVertice);

            buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() + 1, novoVertice); // direita
            getPassoBusca().novoPassoBusca(i, j); // avisa que voltou.

            buscaRecursiva(novoVertice.getLinha() + 1, novoVertice.getColuna(), novoVertice); // baixo
            getPassoBusca().novoPassoBusca(i, j); // avisa que voltou.

            buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() - 1, novoVertice); // esquerda
            getPassoBusca().novoPassoBusca(i, j); // avisa que voltou.

            buscaRecursiva(novoVertice.getLinha() - 1, novoVertice.getColuna(), novoVertice); // cima
            getPassoBusca().novoPassoBusca(i, j); // avisa que voltou.

        }

    }

    @Override
    public void realizarBusca() {

        Vertice novoVertice = new Vertice(getLinhaInicial(), getColunaInicial());
        getGrafo().put(novoVertice, novoVertice);

        buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() + 1, novoVertice); // direita
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou.

        buscaRecursiva(novoVertice.getLinha() + 1, novoVertice.getColuna(), novoVertice); // baixo
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou.

        buscaRecursiva(novoVertice.getLinha(), novoVertice.getColuna() - 1, novoVertice); // esquerda
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou.

        buscaRecursiva(novoVertice.getLinha() - 1, novoVertice.getColuna(), novoVertice); // cima
        getPassoBusca().novoPassoBusca(novoVertice.getLinha(), novoVertice.getColuna()); // avisa que voltou.


    }

}

