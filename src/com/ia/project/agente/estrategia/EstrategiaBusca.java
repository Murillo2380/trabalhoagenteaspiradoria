package com.ia.project.agente.estrategia;

/**
 * Classe que define atributos de uma estrategia em busca.
 */
public interface EstrategiaBusca<T> extends Estrategia{

    /**
     * Realiza um tipo de busca.
     */
    void realizarBusca();

    T resultado();

}
