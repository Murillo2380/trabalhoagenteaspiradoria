package com.ia.project.agente;

import com.ia.project.agente.estrategia.EstrategiaBusca;
import com.ia.project.mapa.Mapa;
import com.ia.project.agente.estrategia.EstrategiaBuscaMatriz;
import com.sun.istack.internal.NotNull;

import java.io.IOException;

/**
 * Agente que se move baseado em um mapa no formato de matriz.
 */
public abstract class AgenteMatriz extends Agente implements EstrategiaBuscaMatriz.PassoBusca, Movimentacao{

    private EstrategiaBuscaMatriz.Grafo grafo;

    private int linhaAtual;
    private int colunaAtual;
    private int numPassos;

    /**
     * @param estrategia Estrategia a ser usada pelo agente para se mover no mapa.
     */
    public AgenteMatriz(@NotNull EstrategiaBuscaMatriz estrategia) {
        super(estrategia);
        linhaAtual = 0;
        colunaAtual = 0;
    }

    /**
     *
     * @return Linha onde o agente se encontra.
     */
    public int getLinhaAtual(){
        return linhaAtual;
    }

    /**
     *
     * @return Coluna onde o agente se encontra.
     */
    public int getColunaAtual(){
        return colunaAtual;
    }

    /**
     *
     * @return Número de passos realizados pelo agente.
     */
    public int getNumPassos(){
        return numPassos;
    }

    @Override
    public void novoPassoBusca(int i, int j) {

        if(i == linhaAtual && j > colunaAtual){
            moverDireita();
        }

        else if(i == linhaAtual && j < colunaAtual){
            moverEsquerda();
        }

        else if (i < linhaAtual && j == colunaAtual){
            moverCima();
        }

        else if (i > linhaAtual && j == colunaAtual){
            moverBaixo();
        }

    }

    @Override
    public void moverCima() {
        linhaAtual--;
        numPassos++;
        imprimir();
    }

    @Override
    public void moverBaixo() {
        linhaAtual++;
        numPassos++;
        imprimir();
    }

    @Override
    public void moverEsquerda() {
        colunaAtual--;
        numPassos++;
        imprimir();
    }

    @Override
    public void moverDireita() {
        colunaAtual++;
        numPassos++;
        imprimir();
    }

    private void imprimir(){
        System.out.print(((EstrategiaBuscaMatriz) getEstrategia()).getMapeamento().toString());

        try {
            System.in.read(); // leia qualquer coisa do teclado
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
